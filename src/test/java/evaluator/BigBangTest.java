package evaluator;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BigBangTest {

    private IntrebariRepository intrebariRepo;
    private AppController controller;

    @Before
    public void setUp() throws Exception {
        intrebariRepo = new IntrebariRepository();
        controller = new AppController();
    }

    @Test
    public void unitTestA() throws Exception {
        Intrebare intrebare = new Intrebare("Enunt?","1)test","2)test","3)test",1,"Test");
        int initialIntrebariCount = intrebariRepo.getNumberOfIntrebari();
        intrebariRepo.addIntrebare(intrebare);
        int finalIntrebariCount = intrebariRepo.getNumberOfIntrebari();

        assertEquals(1, finalIntrebariCount - initialIntrebariCount);
    }

    @Test
    public void unitTestB() throws InputValidationFailedException, DuplicateIntrebareException {
        try {
            Intrebare intrebare1 = new Intrebare("Intrebare1?", "1)", "2)", "3)", 1, "Domeniu1");
            Intrebare intrebare2 = new Intrebare("Intrebare2?", "1)", "2)", "3)", 1, "Domeniu2");
            Intrebare intrebare3 = new Intrebare("Intrebare3?", "1)", "2)", "3)", 1, "Domeniu3");
            Intrebare intrebare4 = new Intrebare("Intrebare4?", "1)", "2)", "3)", 1, "Domeniu4");
            Intrebare intrebare5 = new Intrebare("Intrebare5?", "1)", "2)", "3)", 1, "Domeniu5");

            controller.addNewIntrebare(intrebare1);
            controller.addNewIntrebare(intrebare2);
            controller.addNewIntrebare(intrebare3);
            controller.addNewIntrebare(intrebare4);
            controller.addNewIntrebare(intrebare5);

            evaluator.model.Test generatedTest = controller.createNewTest();
            List<Intrebare> generatedTestQuestions = generatedTest.getIntrebari();

            if (generatedTestQuestions.size() == 5 &&
                    generatedTestQuestions.contains(intrebare1) &&
                    generatedTestQuestions.contains(intrebare2) &&
                    generatedTestQuestions.contains(intrebare3) &&
                    generatedTestQuestions.contains(intrebare4) &&
                    generatedTestQuestions.contains(intrebare5)){

                assertTrue(true);
            } else {
                assertFalse(true);
            }

        } catch (NotAbleToCreateTestException ex){
            assertFalse(true);
        }
    }

    @Test
    public void unitTestC() throws InputValidationFailedException, DuplicateIntrebareException {
        try {
            Intrebare intrebare1 = new Intrebare("Intrebare1?", "1)", "2)", "3)", 1, "Domeniu1");
            Intrebare intrebare2 = new Intrebare("Intrebare2?", "1)", "2)", "3)", 1, "Domeniu2");
            Intrebare intrebare3 = new Intrebare("Intrebare3?", "1)", "2)", "3)", 1, "Domeniu3");
            Intrebare intrebare4 = new Intrebare("Intrebare4?", "1)", "2)", "3)", 1, "Domeniu4");
            Intrebare intrebare5 = new Intrebare("Intrebare5?", "1)", "2)", "3)", 1, "Domeniu5");
            controller.addNewIntrebare(intrebare1);
            controller.addNewIntrebare(intrebare2);
            controller.addNewIntrebare(intrebare3);
            controller.addNewIntrebare(intrebare4);
            controller.addNewIntrebare(intrebare5);

            Statistica statistica = controller.getStatistica();
            Map<String, Integer> intrebariDomenii = statistica.getIntrebariDomenii();

            if (intrebariDomenii.size() == 5 &&
                    intrebariDomenii.get("Domeniu1") == 1 &&
                    intrebariDomenii.get("Domeniu2") == 1 &&
                    intrebariDomenii.get("Domeniu3") == 1 &&
                    intrebariDomenii.get("Domeniu4") == 1 &&
                    intrebariDomenii.get("Domeniu5") == 1){
                assertTrue(true);
            } else {
                assertFalse(true);
            }

        } catch (NotAbleToCreateStatisticsException e) {
            assertFalse(true);
        }
    }

    @Test
    public void integrationTestABC() {
        try {
            Intrebare intrebare1 = new Intrebare("Intrebare1?", "1)", "2)", "3)", 1, "Domeniu1");
            Intrebare intrebare2 = new Intrebare("Intrebare2?", "1)", "2)", "3)", 1, "Domeniu2");
            Intrebare intrebare3 = new Intrebare("Intrebare3?", "1)", "2)", "3)", 1, "Domeniu3");
            Intrebare intrebare4 = new Intrebare("Intrebare4?", "1)", "2)", "3)", 1, "Domeniu4");
            Intrebare intrebare5 = new Intrebare("Intrebare5?", "1)", "2)", "3)", 1, "Domeniu5");

            // add intrebari
            controller.addNewIntrebare(intrebare1);
            controller.addNewIntrebare(intrebare2);
            controller.addNewIntrebare(intrebare3);
            controller.addNewIntrebare(intrebare4);
            controller.addNewIntrebare(intrebare5);

            // create test
            evaluator.model.Test generatedTest = controller.createNewTest();
            List<Intrebare> generatedTestQuestions = generatedTest.getIntrebari();

            if (generatedTestQuestions.size() == 5 &&
                    generatedTestQuestions.contains(intrebare1) &&
                    generatedTestQuestions.contains(intrebare2) &&
                    generatedTestQuestions.contains(intrebare3) &&
                    generatedTestQuestions.contains(intrebare4) &&
                    generatedTestQuestions.contains(intrebare5)) {

                // create statistica

                Statistica statistica = controller.getStatistica();
                Map<String, Integer> intrebariDomenii = statistica.getIntrebariDomenii();

                if (intrebariDomenii.get("Domeniu1") == 1 &&
                        intrebariDomenii.get("Domeniu2") == 1 &&
                        intrebariDomenii.get("Domeniu3") == 1 &&
                        intrebariDomenii.get("Domeniu4") == 1 &&
                        intrebariDomenii.get("Domeniu5") == 1) {
                    assertTrue(true);
                } else {
                    assertFalse(true);
                }

            } else {
                assertFalse(true);
            }
        } catch (InputValidationFailedException | NotAbleToCreateStatisticsException | NotAbleToCreateTestException | DuplicateIntrebareException e) {
            assertFalse(true);
        }
    }

}

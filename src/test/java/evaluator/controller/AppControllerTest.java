package evaluator.controller;

import evaluator.controller.AppController;
import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateStatisticsException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;
import evaluator.repository.IntrebariRepository;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AppControllerTest {

    private AppController appController;

    @Before
    public void setUp() throws Exception {
        appController = new AppController();
    }

    @Test
    public void testFailCreareTestPreaPutineIntrebari() {
        try {
            appController.createNewTest();
            assertFalse(true);
        } catch (NotAbleToCreateTestException ex){
            if (ex.getMessage().equals("Nu exista suficiente intrebari pentru crearea unui test!(5)")){
                assertTrue(true);

            } else {
                assertFalse(true);
            }
        }
    }

    @Test
    public void testFailCreareTestSuficienteIntrebariPreaPutineDomenii() throws InputValidationFailedException, DuplicateIntrebareException {
        try {
            appController.addNewIntrebare(new Intrebare("Intrebare1?", "1)", "2)", "3)", 1, "Domeniu1"));
            appController.addNewIntrebare(new Intrebare("Intrebare2?", "1)", "2)", "3)", 1, "Domeniu1"));
            appController.addNewIntrebare(new Intrebare("Intrebare3?", "1)", "2)", "3)", 1, "Domeniu1"));
            appController.addNewIntrebare(new Intrebare("Intrebare4?", "1)", "2)", "3)", 1, "Domeniu1"));
            appController.addNewIntrebare(new Intrebare("Intrebare5?", "1)", "2)", "3)", 1, "Domeniu1"));
            appController.createNewTest();
            assertFalse(true);
        } catch (NotAbleToCreateTestException ex){
            if (ex.getMessage().equals("Nu exista suficiente domenii pentru crearea unui test!(5)")){
                assertTrue(true);

            } else {
                assertFalse(true);
            }
        }
    }

    @Test
    public void testSuccesCreareTest() throws InputValidationFailedException, DuplicateIntrebareException {
        try {
            Intrebare intrebare1 = new Intrebare("Intrebare1?", "1)", "2)", "3)", 1, "Domeniu1");
            Intrebare intrebare2 = new Intrebare("Intrebare2?", "1)", "2)", "3)", 1, "Domeniu2");
            Intrebare intrebare3 = new Intrebare("Intrebare3?", "1)", "2)", "3)", 1, "Domeniu3");
            Intrebare intrebare4 = new Intrebare("Intrebare4?", "1)", "2)", "3)", 1, "Domeniu4");
            Intrebare intrebare5 = new Intrebare("Intrebare5?", "1)", "2)", "3)", 1, "Domeniu5");

            appController.addNewIntrebare(intrebare1);
            appController.addNewIntrebare(intrebare2);
            appController.addNewIntrebare(intrebare3);
            appController.addNewIntrebare(intrebare4);
            appController.addNewIntrebare(intrebare5);

            evaluator.model.Test generatedTest = appController.createNewTest();
            List<Intrebare> generatedTestQuestions = generatedTest.getIntrebari();

            if (generatedTestQuestions.size() == 5 &&
                    generatedTestQuestions.contains(intrebare1) &&
                    generatedTestQuestions.contains(intrebare2) &&
                    generatedTestQuestions.contains(intrebare3) &&
                    generatedTestQuestions.contains(intrebare4) &&
                    generatedTestQuestions.contains(intrebare5)){

                    assertTrue(true);
            } else {
                assertFalse(true);
            }

        } catch (NotAbleToCreateTestException ex){
            assertFalse(true);
        }
    }

    @Test
    public void testSuccesCreareStatistica() throws InputValidationFailedException, DuplicateIntrebareException {
        try {
            Intrebare intrebare1 = new Intrebare("Intrebare1?", "1)", "2)", "3)", 1, "Domeniu1");
            Intrebare intrebare2 = new Intrebare("Intrebare2?", "1)", "2)", "3)", 1, "Domeniu2");
            Intrebare intrebare3 = new Intrebare("Intrebare3?", "1)", "2)", "3)", 1, "Domeniu3");
            Intrebare intrebare4 = new Intrebare("Intrebare4?", "1)", "2)", "3)", 1, "Domeniu4");
            Intrebare intrebare5 = new Intrebare("Intrebare5?", "1)", "2)", "3)", 1, "Domeniu5");
            appController.addNewIntrebare(intrebare1);
            appController.addNewIntrebare(intrebare2);
            appController.addNewIntrebare(intrebare3);
            appController.addNewIntrebare(intrebare4);
            appController.addNewIntrebare(intrebare5);

            Statistica statistica = appController.getStatistica();
            Map<String, Integer> intrebariDomenii = statistica.getIntrebariDomenii();

            if (intrebariDomenii.get("Domeniu1") == 1 &&
                    intrebariDomenii.get("Domeniu2") == 1 &&
                    intrebariDomenii.get("Domeniu3") == 1 &&
                    intrebariDomenii.get("Domeniu4") == 1 &&
                    intrebariDomenii.get("Domeniu5") == 1){
                assertTrue(true);
            } else {
                assertFalse(true);
            }

        } catch (NotAbleToCreateStatisticsException e) {
            assertFalse(true);
        }
    }

    @Test
    public void testFailCreareStatisticaEmptyRepo() throws InputValidationFailedException, DuplicateIntrebareException {
        try {
            Statistica statistica = appController.getStatistica();
            assertFalse(true);
        } catch (NotAbleToCreateStatisticsException e) {
            if (e.getMessage().equals("Repository-ul nu contine nicio intrebare!")){
                assertTrue(true);
            } else {
                assertFalse(true);
            }
        }
    }
}

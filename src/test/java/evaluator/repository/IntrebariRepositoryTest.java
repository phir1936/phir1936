package evaluator.repository;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.model.Intrebare;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class IntrebariRepositoryTest {

    private IntrebariRepository intrebariRepository;

    @Before
    public void setUp() throws Exception {
        intrebariRepository = new IntrebariRepository();
    }

    @Test
    public void testEnuntValid() throws Exception {
        Intrebare intrebare = new Intrebare("Enunt corect?","1)test","2)test","3)test",1,"Test");
        int initialIntrebariCount = intrebariRepository.getNumberOfIntrebari();
        intrebariRepository.addIntrebare(intrebare);
        int finalIntrebariCount = intrebariRepository.getNumberOfIntrebari();

        assertEquals(1, finalIntrebariCount - initialIntrebariCount);
    }

    @Test
    public void testEnuntInvalidLiteraMicaLaInceput()  throws Exception {
        try {
            Intrebare intrebare = new Intrebare("enunt?", "1)test", "2)test", "3)test", 1, "Test");
            intrebariRepository.addIntrebare(intrebare);
            assertFalse(true);

        } catch(InputValidationFailedException ex){
            if (ex.getMessage().equals("Prima litera din enunt nu e majuscula!")){
                assertTrue(true);

            } else {
                assertFalse(true);
            }
        }
    }

    @Test
    public void testEnuntInvalidNotString() throws Exception {
        try {
            Intrebare intrebare = new Intrebare("","1)test","2)test","3)test",1,"Test");
            intrebariRepository.addIntrebare(intrebare);
            assertFalse(true);

        } catch(InputValidationFailedException ex){
            if (ex.getMessage().equals("Enuntul este vid!")){
                assertTrue(true);

            } else {
                assertFalse(true);
            }
        }
    }

    @Test
    public void testEnuntInvalidLiteraMareFaraSemnIntrebare() throws Exception {
        try{
            Intrebare intrebare = new Intrebare("Enunt","1)test","2)test","3)test",1,"Test");
            intrebariRepository.addIntrebare(intrebare);
            assertFalse(true);

        } catch(InputValidationFailedException ex){
            if (ex.getMessage().equals("Ultimul caracter din enunt nu e '?'!")){
                assertTrue(true);

            } else {
                assertFalse(true);
            }
        }
    }

    @Test
    public void testVarianta1Invalid() throws Exception {
        try {
            Intrebare intrebare = new Intrebare("Enunt?", "v1", "2)test", "3)test", 1, "Test");
            intrebariRepository.addIntrebare(intrebare);
            assertFalse(true);

        } catch(InputValidationFailedException ex){
            if (ex.getMessage().equals("Varianta1 nu incepe cu '1)'!")){
                assertTrue(true);

            } else {
                assertFalse(true);
            }
        }
    }

    @Test
    public void testRaspunsCorectValid1() throws Exception {
        Intrebare intrebare = new Intrebare("Enunt?","1)test","2)test","3)test",1,"Test");
        int initialIntrebariCount = intrebariRepository.getNumberOfIntrebari();
        intrebariRepository.addIntrebare(intrebare);
        int finalIntrebariCount = intrebariRepository.getNumberOfIntrebari();

        assertEquals(1, finalIntrebariCount - initialIntrebariCount);
    }

    @Test
    public void testRaspunsCorectValid2() throws Exception {
        Intrebare intrebare = new Intrebare("Enunt?","1)test","2)test","3)test",2,"Test");
        int initialIntrebariCount = intrebariRepository.getNumberOfIntrebari();
        intrebariRepository.addIntrebare(intrebare);
        int finalIntrebariCount = intrebariRepository.getNumberOfIntrebari();

        assertEquals(1, finalIntrebariCount - initialIntrebariCount);
    }

    @Test
    public void testRaspunsCorectValid3() throws Exception {
        Intrebare intrebare = new Intrebare("Enunt?","1)test","2)test","3)test",3,"Test");
        int initialIntrebariCount = intrebariRepository.getNumberOfIntrebari();
        intrebariRepository.addIntrebare(intrebare);
        int finalIntrebariCount = intrebariRepository.getNumberOfIntrebari();

        assertEquals(1, finalIntrebariCount - initialIntrebariCount);
    }

    @Test
    public void testRaspunsCorectInvalidm1() throws Exception {
        try {
            Intrebare intrebare = new Intrebare("Enunt?", "1)test", "2)test", "3)test", -1, "Test");
            intrebariRepository.addIntrebare(intrebare);
            assertFalse(true);

        } catch(InputValidationFailedException ex){
            if (ex.getMessage().equals("Varianta corecta nu este 1, 2 sau 3")){
                assertTrue(true);

            } else {
                assertFalse(true);
            }
        }
    }

    @Test
    public void testRaspunsCorectInvalid0() throws Exception {
        try{
            Intrebare intrebare = new Intrebare("Enunt?","1)test","2)test","3)test",0,"Test");
            intrebariRepository.addIntrebare(intrebare);
            assertFalse(true);

        } catch(InputValidationFailedException ex){
            if (ex.getMessage().equals("Varianta corecta nu este 1, 2 sau 3")){
                assertTrue(true);

            } else {
                assertFalse(true);
            }
        }
    }

    @Test
    public void testRaspunsCorectInvalid4() throws Exception {
        try{
            Intrebare intrebare = new Intrebare("Enunt?","1)test","2)test","3)test",4,"Test");
            intrebariRepository.addIntrebare(intrebare);
            assertFalse(true);

        } catch(InputValidationFailedException ex){
            if (ex.getMessage().equals("Varianta corecta nu este 1, 2 sau 3")){
                assertTrue(true);

            } else {
                assertFalse(true);
            }
        }
    }

    @Test
    public void testRaspunsCorectInvalid5() throws Exception {
        try{
            Intrebare intrebare = new Intrebare("Enunt?","1)test","2)test","3)test",5,"Test");
            intrebariRepository.addIntrebare(intrebare);
            assertFalse(true);

        } catch(InputValidationFailedException ex){
            if (ex.getMessage().equals("Varianta corecta nu este 1, 2 sau 3")){
                assertTrue(true);

            } else {
                assertFalse(true);
            }
        }
    }

}
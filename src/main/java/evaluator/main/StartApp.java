package evaluator.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import evaluator.exception.DuplicateIntrebareException;
import evaluator.exception.InputValidationFailedException;
import evaluator.exception.NotAbleToCreateTestException;
import evaluator.model.Intrebare;
import evaluator.model.Statistica;

import evaluator.controller.AppController;
import evaluator.exception.NotAbleToCreateStatisticsException;

//functionalitati
//i.	 adaugarea unei noi intrebari pentru un anumit domeniu (enunt intrebare, raspuns 1, raspuns 2, raspuns 3, raspunsul corect, domeniul) in setul de intrebari disponibile;
//ii.	 crearea unui nou test (testul va contine 5 intrebari alese aleator din cele disponibile, din domenii diferite);
//iii.	 afisarea unei statistici cu numarul de intrebari organizate pe domenii.

public class StartApp {

	private static final String file = "C:\\Users\\Andrei\\dev\\VVSS\\05-ProiectEvaluatorExamen\\ProiectEvaluatorExamen\\src\\main\\java\\intrebari.txt";
	
	public static void main(String[] args) throws IOException {
		
		BufferedReader console = new BufferedReader(new InputStreamReader(System.in));
		
		AppController appController = new AppController();
		
		boolean activ = true;
		int optiune;
		
		while(activ){
			
			System.out.println("");
			System.out.println("1.Adauga intrebare");
			System.out.println("2.Creeaza test");
			System.out.println("3.Statistica");
			System.out.println("4.Exit");
			System.out.println("");
			
			optiune = Integer.parseInt(console.readLine());

            appController.loadIntrebariFromFile(file);
			
			switch(optiune){
			case 1 :
                System.out.println("enunt:");
                String enunt = console.readLine();

                System.out.println("v1:");
                String v1 = console.readLine();

                System.out.println("v2:");
                String v2 = console.readLine();

                System.out.println("v3:");
                String v3 = console.readLine();

                System.out.println("vCorecta:");
                int vCorecta = Integer.parseInt(console.readLine());

                System.out.println("Domeniu");
                String domeniu = console.readLine();

                try {
                    System.out.println(appController.addNewIntrebare(new Intrebare(enunt, v1, v2, v3, vCorecta, domeniu)));
                } catch (DuplicateIntrebareException | InputValidationFailedException e) {
                    System.out.println(e.toString() + "; message = " + e.getMessage());
                }

                break;

			case 2 :
                try {
                    System.out.println(appController.createNewTest().toString());
                } catch (NotAbleToCreateTestException e) {
                    System.out.println(e.toString() + "; message = " + e.getMessage());
                }
                break;

			case 3 :

				Statistica statistica;
				try {
					statistica = appController.getStatistica();
					System.out.println(statistica);
				} catch (NotAbleToCreateStatisticsException e) {
                    System.out.println(e.getMessage());
                }
				
				break;
			case 4 :
				activ = false;
				break;
			default:
				break;
			}
		}
		
	}

}
